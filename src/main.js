import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import './plugins'
import store from './store'
import { sync } from 'vuex-router-sync'
import i18n from './i18n'
Vue.config.productionTip = false
sync(store, router)

router.beforeEach((to, from , next) => {

  if (to.path === '/' && localStorage.getItem('user') !== null) {
    next({name: 'pedidos'})
  } else if (to.path === '/' && localStorage.getItem('user') === null) {
    next({name: 'Login'})
  }
  if (to.path === '/login' || from.path === '/login' || to.path === '/403') {
    next()
  } else {
    if (localStorage.getItem('user') === null) {
      next({name: 'Login'})
    } else {
      let user = localStorage.getItem('user');
      console.log(user)
      // Vue.prototype.$permiso = user.tipo
      let forArray = to.meta.for ?? []
      if (forArray.includes(user)) {
        next()
      } else {
        next({
          name: '403',
        })
      }
    }
  }
})


new Vue({
  router,
  vuetify,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
