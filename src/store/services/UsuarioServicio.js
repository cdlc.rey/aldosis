import ApiService from "./ApiService";

class UsuarioServicio extends ApiService
{
  constructor() {
    super("/usuarios");
  }
  index(params) {
    if (params)
      return this.service.get(`${this.resource}`, { params });
    return this.service.get(`${this.resource}`);
  }
}
export default new UsuarioServicio();
