import ApiService from "./ApiService";

class PedidoServicio extends ApiService
{
  constructor() {
    super("/pedidos");
  }
  asignCompra(pedidoId, compraId) {
    return this.service.get(`${this.resource}/${pedidoId}/compra/${compraId}`);
  }
  index(params) {
    if (params)
      return this.service.get(`${this.resource}`, { params });
    return this.service.get(`${this.resource}`);
  }
  reporte(url, params) {
    return this.service.get(`${this.resource}/${url}`, params);
  }
  cambiarEstado(pedidoId, payload) {
    return this.service.put(`${this.resource}/${pedidoId}/cambiar-estado`, payload)
  }
  detalleTotal(pedidoId) {
    return this.service.get(`${this.resource}/${pedidoId}/detalle`)
  }
  terminar(pedidoId, payload, headers) {
    return this.service.post(`${this.resource}/${pedidoId}/terminar`, {payload}, headers)
  }
}
export default new PedidoServicio();
