import ApiService from "./ApiService";

class DetalleRecepcionServicio extends ApiService
{
  constructor() {
    super("/detalle_recepciones");
  }
  index(params) {
    if (params)
      return this.service.get(`${this.resource}`, { params });
    return this.service.get(`${this.resource}`);
  }
}
export default new DetalleRecepcionServicio();
