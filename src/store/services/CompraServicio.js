import ApiService from "./ApiService";

class CompraServicio extends ApiService
{
  constructor() {
    super("/compras");
  }
  index(params) {
    if (params)
      return this.service.get(`${this.resource}`, { params });
    return this.service.get(`${this.resource}`);
  }
}
export default new CompraServicio();
