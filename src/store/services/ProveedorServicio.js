import ApiService from "./ApiService";

class ProveedorServicio extends ApiService
{
  constructor() {
    super("/proveedores");
  }
  index(params) {
    if (params)
      return this.service.get(`${this.resource}`, { params });
    return this.service.get(`${this.resource}`);
  }
}
export default new ProveedorServicio();
