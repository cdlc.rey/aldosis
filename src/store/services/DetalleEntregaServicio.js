import ApiService from "./ApiService";

class DetalleEntregaServicio extends ApiService
{
  constructor() {
    super("/detalle_entregas");
  }
  index(params) {
    if (params)
      return this.service.get(`${this.resource}`, { params });
    return this.service.get(`${this.resource}`);
  }
}
export default new DetalleEntregaServicio();
