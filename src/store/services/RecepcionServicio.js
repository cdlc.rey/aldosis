import ApiService from "./ApiService";

class RecepcionServicio extends ApiService
{
  constructor() {
    super("/recepciones");
  }
  index(params) {
    if (params)
      return this.service.get(`${this.resource}`, { params });
    return this.service.get(`${this.resource}`);
  }
  asignCompra(recepcionId, compraId) {
    return this.service.get(`${this.resource}/${recepcionId}/compra/${compraId}`);
  }
  imprimir(recepcionId, params) {
    return this.service.get(`${this.resource}/${recepcionId}/imprimir`, params)
  }
}
export default new RecepcionServicio();
