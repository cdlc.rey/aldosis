import ApiService from "./ApiService";

class EntregaServicio extends ApiService
{
  constructor() {
    super("/entregas");
  }
  index(params) {
    if (params)
      return this.service.get(`${this.resource}`, { params });
    return this.service.get(`${this.resource}`);
  }
}
export default new EntregaServicio();
