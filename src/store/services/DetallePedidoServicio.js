import ApiService from "./ApiService";

class DetallePedidoServicio extends ApiService
{
  constructor() {
    super("/detalle_pedidos");
  }
  index(params) {
    if (params)
      return this.service.get(`${this.resource}`, { params });
    return this.service.get(`${this.resource}`);
  }
}
export default new DetallePedidoServicio();
