import ApiService from "./ApiService";

class DetalleCompraServicio extends ApiService
{
  constructor() {
    super("/detalle_compras");
  }
  index(params) {
    if (params)
      return this.service.get(`${this.resource}`, { params });
    return this.service.get(`${this.resource}`);
  }
}
export default new DetalleCompraServicio();
