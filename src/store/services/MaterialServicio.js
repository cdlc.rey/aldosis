import ApiService from "./ApiService";

class MaterialServicio extends ApiService
{
  constructor() {
    super("/materiales");
  }
  index(params) {
    if (params)
      return this.service.get(`${this.resource}`, { params });
    return this.service.get(`${this.resource}`);
  }
  kardex(materialId, params) {
    return this.service.get(`${this.resource}/${materialId}/kardex`, params)
  }
}
export default new MaterialServicio();
