import api from '@/api'
import router from '../../router'

export default {
    namespaced: true,
    state: {
        authenticated: false,
        user: null,
    },

    getters: {
        authenticated (state) {
            return state.authenticated
        },
        user (state) {
            return state.user
        },
    },

    mutations: {
        SET_AUTHENTICATED (state, value) {
            state.authenticated = value
        },
        SET_USER (state, value) {
            state.user = value
        },
    },

    actions: {
        async signIn ({ dispatch }, credentials) {
            // const headers = {
            //       'Content-Type': 'application/Json',
            // };
            const data = await api.post('/api/login', credentials)
            dispatch('setSession', data.data)
            return data
        },

        async signOut ({ dispatch }) {
            await api.post('/logout')
            return dispatch('eliminarSesion')
        },

        setSession ({ commit }, data) {
            localStorage.setItem('access_token', data.access_token)
            localStorage.setItem('token_type', data.token_type)
            localStorage.setItem('user', data.user.tipo)
            commit('SET_AUTHENTICATED', true)
            commit('SET_USER', data.user)
        },

        eliminarSesion ({ commit }) {
          localStorage.setItem('access_token', null)
          localStorage.setItem('token_type', null)
          localStorage.setItem('user', null)
          commit('SET_AUTHENTICATED', false)
          commit('SET_USER', null)
          router.push('/login')
        }
    },
}
