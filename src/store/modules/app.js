import { make } from 'vuex-pathify'

// Data
const state = {
  drawer: null,
  drawerImage: true,
  mini: false,
  items: [
    // {
    //   title: 'Dashboard',
    //   icon: 'mdi-view-dashboard',
    //   to: '/dashboard',
    //   for: ['ADMIN', 'COMPRADOR', 'ALMACEN', 'SOLICITANTE', 'JEFATURA'],
    // },
    {
      title: 'Materiales',
      icon: 'mdi-palette-swatch-variant',
      to: '/materiales',
      for: ['ADMIN', 'ALMACEN']
    },
    {
      title: 'Proveedores',
      icon: 'mdi-handshake',
      to: '/proveedores',
      for: ['ADMIN', 'ALMACEN']
    },
    {
      title: 'Pedidos',
      icon: 'mdi-cart',
      // to: '/components/typography/',
      for: ['ADMIN', 'ALMACEN', 'SOLICITANTE', 'JEFATURA', 'COMPRADOR'],
      items: [
        {
          title: 'Lista Pedidos',
          icon: 'mdi-cart-arrow-right',
          to: '/pedidos',
          for: ['ADMIN', 'ALMACEN', 'SOLICITANTE', 'JEFATURA', 'COMPRADOR']
        },
        {
          title: 'Nuevo Pedido',
          icon: 'mdi-cart-plus',
          to: '/nuevo-pedido',
          for: ['ADMIN', 'ALMACEN', 'SOLICITANTE', 'JEFATURA', 'COMPRADOR']
        }
      ]
    },
    {
      title: 'Compras',
      icon: 'mdi-truck-delivery',
      // to: '/components/typography/',
      for: ['ADMIN', 'COMPRADOR'],
      items: [
        {
          title: 'Lista Compras',
          icon: 'mdi-format-list-bulleted-square',
          to: '/compras',
          for: ['ADMIN', 'COMPRADOR']
        },
        {
          title: 'Nueva Compras',
          icon: 'mdi-cart-arrow-down',
          to: '/nueva-compra',
          for: ['ADMIN', 'COMPRADOR']
        }
      ]
    },
    {
      title: 'Recepciones',
      icon: 'mdi-archive',
      // to: '/components/typography/',
      for: ['ADMIN', 'ALMACEN'],
      items: [
        {
          title: 'Lista Recepciones',
          icon: 'mdi-inbox-full',
          to: '/recepciones',
          for: ['ADMIN', 'ALMACEN'],
        },
        {
          title: 'Nueva Recepcion',
          icon: 'mdi-archive-plus',
          to: '/nueva-recepcion',
          for: ['ADMIN', 'ALMACEN'],
        }
      ]
    },
    {
      title: 'Usuarios',
      icon: 'mdi-account',
      to: '/usuarios',
      for: ['ADMIN'],
      // items: [
      //   {
      //     title: 'Usuarios',
      //     icon: 'mdi-cart',
      //     to: '/usuarios',
      //   }
      // ]
    },
    // {
    //   title: 'Entregas',
    //   icon: 'mdi-cart',
    //   // to: '/components/typography/',
    //   items: [
    //     {
    //       title: 'Lista Entregas',
    //       icon: 'mdi-cart',
    //       to: '/entregas',
    //     },
    //     {
    //       title: 'Nueva Entrega',
    //       icon: 'mdi-cart',
    //       to: '/nueva-entrega',
    //     }
    //   ]
    // },
  ],
}

const mutations = make.mutations(state)

const actions = {
  ...make.actions(state),
  init: async () => {
    //
  },
}

const getters = {}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
}
