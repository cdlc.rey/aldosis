import Vue from 'vue'
import VueRouter from 'vue-router'
import misRutas from './misRutas'

Vue.use(VueRouter)

const routes = [
  misRutas,

  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/Login.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
