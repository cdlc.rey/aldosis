import Layout from '@/layouts/default/Index.vue';

export default {
  path: '/',
  component: Layout,
  // redirect: '/',
  children: [
    {
      path: '/dashboard',
      name: 'dashboard',
      component: () => import('@/views/Dashboard.vue'),
      meta: {
        free: true,
        title: 'message',
        for: ['ADMIN', 'COMPRADOR', 'ALMACEN', 'JEFATURA', 'SOLICITANTE'],
      },
    },
    {
      path: '/materiales',
      component: () => import('@/views/Materiales/VistaMateriales'),
      name: 'materiales',
      meta: {
        free: true,
        title: 'materiales',
        for: ['ADMIN', 'ALMACEN'],
      },
    },
    {
      path: '/proveedores',
      component: () => import('@/views/Proveedores/VistaProveedores'),
      name: 'proveedores',
      meta: {
        free: true,
        title: 'proveedores',
        for: ['ADMIN', 'ALMACEN']
      },
    },
    {
      path: '/pedidos',
      component: () => import('@/views/Pedidos/VistaPedidos'),
      name: 'pedidos',
      meta: {
        free: true,
        title: 'pedidos',
        for: ['ADMIN', 'ALMACEN', 'SOLICITANTE', 'JEFATURA', 'COMPRADOR'],
      },
    },
    {
      path: '/nuevo-pedido',
      component: () => import('@/views/Pedidos/VistaNuevoPedido'),
      name: 'nuevoPedido',
      meta: {
        free: true,
        title: 'nuevoPedido',
        for: ['ADMIN', 'ALMACEN', 'SOLICITANTE', 'JEFATURA', 'COMPRADOR'],
      },
    },
    {
      path: '/compras',
      component: () => import('@/views/Compras/VistaCompras'),
      name: 'compras',
      meta: {
        free: true,
        title: 'compras',
        for: ['ADMIN', 'COMPRADOR'],
      },
    },
    {
      path: '/nueva-compra',
      component: () => import('@/views/Compras/VistaNuevaCompra'),
      name: 'nuevaCompra',
      meta: {
        free: true,
        title: 'nuevaCompra',
        for: ['ADMIN', 'COMPRADOR'],
      },
    },
    {
      path: '/recepciones',
      component: () => import('@/views/Recepciones/VistaRecepciones'),
      name: 'recepciones',
      meta: {
        free: true,
        title: 'recepciones',
        for: ['ADMIN', 'ALMACEN'],
      },
    },
    {
      path: '/nueva-recepcion',
      component: () => import('@/views/Recepciones/VistaNuevaRecepcion'),
      name: 'nuevaRecepcion',
      meta: {
        free: true,
        title: 'nuevaRecepcion',
        for: ['ADMIN', 'ALMACEN'],
      },
    },
    {
      path: '/entregas/:id',
      component: () => import('@/views/Entregas/VistaEntregas'),
      name: 'entregas',
      meta: {
        free: true,
        title: 'entregas',
          for: ['ADMIN', 'ALMACEN'],
      },
    },
    {
      path: '/nueva-entrega',
      component: () => import('@/views/Entregas/VistaNuevaEntrega'),
      name: 'nuevaEntrega',
      meta: {
        free: true,
        title: 'nuevaEntrega',
        for: ['ADMIN', 'ALMACEN'],
      },
    },
    {
      path: '/usuarios',
      component: () => import('@/views/Usuarios/VistaUsuarios'),
      name: 'usuarios',
      meta: {
        free: true,
        title: 'usuarios',
        for: ['ADMIN'],
      },
    },
    {
      path: '/403',
      component: () => import('@/views/Vista403'),
      name: '403',
      meta: {
        free: true,
        title: '403',
        for: ['ADMIN', 'ALMACEN', 'SOLICITANTE', 'JEFATURA', 'COMPRADOR']
      },
    }
  ]
};
