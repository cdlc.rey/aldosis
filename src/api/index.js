import axios from 'axios'
// import Vue from 'vue'
import router from '../router'
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
}

const instance = axios.create({
  baseURL: process.env.NODE_ENV === 'production' ? process.env.VUE_APP_URL_PROD : process.env.VUE_APP_URL_DEV,
  headers,
})

// Add a request interceptor
instance.interceptors.request.use(function (config) {
  // console.log(config)
  if (config.url !== '/api/login') {
    // Set base url with version api
    config.baseURL = config.baseURL + 'api'
    // Set axios with token authorization
    config.headers['Authorization'] = localStorage.getItem('token_type') + ' ' + localStorage.getItem('access_token')
  }

  // Do something before request is sent
  return config
}, function (error) {
  // Do something with request error
  return Promise.reject(error)
})

// Add a response interceptor
instance.interceptors.response.use(function (response) {
  // Any status code that lie within the range of 2xx cause this function to trigger
  // Do something with response data
  return response
}, function (error) {
  const status = error.response ? error.response.status : null
  alert(error.response.data.message)
  // console.log(error.response)
  // console.log(status)
  if (status === 401) {
    console.log('ESTATUR: '+status)
    router.push('/login')

  }
  // if (status === 400) {
  //   // recall
  //   Vue.swal('Error', error.response.data.message, 'error')
  // } else if (status === 422) {
  //   Vue.swal({
  //     title: 'Error',
  //     type: 'error',
  //     text: Object.entries(error.response.data.errors),
  //   })
  // }

  // Any status codes that falls outside the range of 2xx cause this function to trigger
  // Do something with response error
  return Promise.reject(error)
})

export default instance
